

<div class="top-fix">

	<div class="row top-nav">   


		@if(Auth::check())
		<div class="container nav-pad">            
			<div class="dropdown">
				<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" data-hover="dropdown">
					@if(Auth::check()) {{Auth::user()->name}} @else User @endif <span class="caret"></span>
				</button>
				<ul class="dropdown-menu dropdown-menu-right">
					<li><a href="{{route('user.profile')}}">{{tr('profile')}}</a></li>
					<li><a href="{{route('user.wishlist')}}">{{tr('wishlist')}}</a></li>                
					<li>
						<a href="{{route('user.delete.account')}}" @if(Auth::user()->login_by != 'manual') onclick="return confirm('Are you sure? . Once you deleted account, you will lose your history and wishlist details.')" @endif>
							<i class="fa fa-trash"></i>{{tr('delete_account')}}
						</a>

					</li>  

					<li><a href="{{route('user.logout')}}">{{tr('logout')}}</a></li>                
				</ul>
			</div>  
		</div>

		@else 

		<div class="container nav-pad">
			<a href="{{ route('user.login.form') }}">{{tr('login')}}</a>  
			<a href="{{route('user.register.form')}}">{{tr('register')}}</a>  
		</div>

	</div>


	<div class="row main-nav">

		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">

			<div class="container nav-pad">
				<!-- Brand and toggle get grouped for better mobile display -->
				
				<nav class="navbar navbar-inverse">
					<div class="container-fluid">
						
						<ul class="nav navbar-nav">
							<li class="dropdown">
								<a class="dropdown-toggle" data-toggle="dropdown" href="">Work Out
									<span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li><a href="{{route('user.dashboard')}}">Series 1 (Body weight)</a></li>
										<li><a href="{{route('user.dashboard')}}">Series 2 (Con implementos)</a></li>
									</ul>
								</li>
								<li><a href="{{route('user.dashboard')}}">Warm up</a></li>
								<li><a href="{{route('user.dashboard')}}">Tabata</a></li>
								<li><a href="{{route('user.dashboard')}}">Stretching</a></li>
							</ul>
						</div>
					</nav>
				</div>
			</nav>
		</div>

	</div>


