<?php

use Illuminate\Database\Seeder;

class GenresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genres')->insert([
    		'category_id' => '1',
    		'sub_category_id' => '2',
    		'name' => 'Nature',
    		'position' => 'Nothing',
    		'status' => 'Approved',
    	]);
    }
}
