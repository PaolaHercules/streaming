<?php

use Illuminate\Database\Seeder;

class SubCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('sub_categories')->insert([
    		'category_id' => '1',
    		'name' => 'Beachs',
    		'description' => 'A short description to show',
    		'status' => 'Aprobed',
    	]); 
    }
}
