<?php

use Illuminate\Database\Seeder;

class AdminVideosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
    	DB::table('admin_videos')->insert([
    		'title' => 'Landscapes',
    		'description' => 'A little description',
    		'category_id' => '1',
    		'sub_category_id' => '',
    		'genre_id' => '',
    		'video' => '',
    		'trailer_video' => '',
    		'ratings' => '',
    		'reviews' => '',
    	]);

    }
}
